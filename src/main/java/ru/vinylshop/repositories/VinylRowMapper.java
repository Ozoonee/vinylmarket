package ru.vinylshop.repositories;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import ru.vinylshop.entity.Artist;
import ru.vinylshop.entity.Genre;
import ru.vinylshop.entity.Vinyl;

import java.sql.ResultSet;
import java.sql.SQLException;


public class VinylRowMapper extends BeanPropertyRowMapper<Vinyl> {
    public VinylRowMapper(){
        super(Vinyl.class);
    }

    private BeanPropertyRowMapper<Artist> artistBeanPropertyRowMapper =
            new BeanPropertyRowMapper<Artist>(Artist.class);

    private BeanPropertyRowMapper<Genre> genreBeanPropertyRowMapper =
            new BeanPropertyRowMapper<Genre>(Genre.class);

    @Override
    public Vinyl mapRow(ResultSet rs, int rowNumber) throws SQLException {

        Vinyl vinyl = super.mapRow(rs, rowNumber);
        Artist artist = artistBeanPropertyRowMapper.mapRow(rs, rowNumber);
        vinyl.setArtist(artist);
        Genre genre = genreBeanPropertyRowMapper.mapRow(rs, rowNumber);
        vinyl.setGenre(genre);
        return vinyl;
    }

}
