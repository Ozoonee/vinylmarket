package ru.vinylshop.repositories;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import ru.vinylshop.entity.Token;
import ru.vinylshop.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TokenRawMapper extends BeanPropertyRowMapper<Token> {
    public TokenRawMapper() {
        super(Token.class);
    }
    private BeanPropertyRowMapper<User> userPropertyRowMapper = new UserRowMapper();

    @Override
    public Token mapRow(ResultSet rs, int rowNumber) throws SQLException {
        Token token = super.mapRow(rs, rowNumber);
        User user = userPropertyRowMapper.mapRow(rs, rowNumber);
        token.setUser_id(user.getUserId());
        return token;
    }
}
