package ru.vinylshop.repositories;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.vinylshop.dto.UserDTO;
import ru.vinylshop.util.AuthException;
import ru.vinylshop.util.ServiceMessage;
import java.sql.PreparedStatement;


@Component
public class TokenRepository {
    private static JdbcTemplate jdbcTemplate;
    KeyHolder keyHolder = new GeneratedKeyHolder();
    String SQLRequest = "INSERT INTO users(birth_date, name, address, password, phone, email) VALUES(?, ?, ?, ?, ?, ?) RETURNING user_id";

    public TokenRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int save(UserDTO userDTO) {
        jdbcTemplate.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQLRequest, 1);
            preparedStatement.setDate(1, userDTO.getBirth_Date());
            preparedStatement.setString(2, userDTO.getName());
            preparedStatement.setString(3, userDTO.getAddress());
            preparedStatement.setString(4, userDTO.getPassword());
            preparedStatement.setString(5, userDTO.getPhone());
            preparedStatement.setString(6, userDTO.getEmail());
            return preparedStatement;}, keyHolder);
        return keyHolder.getKey().intValue();
    }

    public static boolean checkUserToken(String token) throws AuthException {
        boolean result = jdbcTemplate.queryForObject("SELECT EXISTS(SELECT FROM tokens WHERE token = ?)", Boolean.class, token);
        if (result == false){
            throw new AuthException(ServiceMessage.AUTH_ERROR);
        }
        return result;
    }
}
