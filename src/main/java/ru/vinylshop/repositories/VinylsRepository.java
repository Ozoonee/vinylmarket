package ru.vinylshop.repositories;


import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.vinylshop.entity.Vinyl;

import java.util.List;

@Component
public class VinylsRepository {

    private JdbcTemplate jdbcTemplate;
    BeanPropertyRowMapper vinylRowMapper = new VinylRowMapper();

    public VinylsRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Vinyl> getVinylList() {
        List<Vinyl> vinylList = jdbcTemplate.query("SELECT v.*, a.*, g.* FROM vinyls v JOIN artists a ON v.vinyl_id = a.artist_id JOIN genres g ON v.vinyl_id = g.genre_id", vinylRowMapper);
        System.out.println(vinylList.toString());
        return vinylList;
    }

    public List<Vinyl> vinylSearch(String name, String artist, String genre) {
        String sqlStr = "SELECT v.name FROM vinyls v WHERE";
        if (name != null && artist == null && genre == null) {
            sqlStr += " v.name LIKE '%" + name + "%'";
        } else if (name != null && artist != null && genre != null) {
            sqlStr += " v.name LIKE '%" + name + "%' AND v.artist_id IN (SELECT a.artist_id FROM artists a where a.name LIKE '%" + artist + "%') AND v.genre_id IN (SELECT g.genre_id FROM genres g where g.name LIKE '%" + genre + "%')";
        } else if (name != null && artist == null && genre != null) {
            sqlStr += " v.name LIKE '%" + name + "%' AND v.genre_id IN (SELECT g.genre_id FROM genres g where g.name LIKE '%" + genre + "%')";
        } else if (name == null && artist != null && genre == null) {
            sqlStr += " v.artist_id IN (SELECT a.artist_id FROM artists a where a.name LIKE '%" + artist + "%')";
        } else if (name != null && artist != null && genre == null) {
            sqlStr += " v.name LIKE '%" + name + "%' AND v.artist_id IN (SELECT a.artist_id FROM artists a where a.name LIKE '%" + artist + "%')";
        } else if (name == null && artist != null && genre != null) {
            sqlStr += " v.artist_id IN (SELECT a.artist_id FROM artists a where a.name LIKE '%" + artist + "%') AND v.genre_id IN (SELECT g.genre_id FROM genres g where g.name LIKE '%" + genre + "%')";
        } else if (name == null && artist == null && genre != null) {
            sqlStr += " v.genre_id IN (SELECT g.genre_id FROM genres g where g.name LIKE '%" + genre + "%')";
        }
        List<Vinyl> vinylList = jdbcTemplate.query(sqlStr, vinylRowMapper);
        return vinylList;
    }
}
