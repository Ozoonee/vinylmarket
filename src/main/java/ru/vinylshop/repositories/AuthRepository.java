package ru.vinylshop.repositories;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.vinylshop.dto.EntranceDTO;
import ru.vinylshop.dto.TokenDTO;
import ru.vinylshop.dto.UserDTO;
import ru.vinylshop.entity.Token;
import ru.vinylshop.service.TokenService;
import ru.vinylshop.util.ServiceMessage;
import ru.vinylshop.util.VinylShopExc;
import java.util.List;

@Component
public class AuthRepository {
    private static JdbcTemplate jdbcTemplate;
    TokenService tokenService = new TokenService();
    TokenDTO tokenDTO = new TokenDTO();
    TokenRepository tokenRepository = new TokenRepository(jdbcTemplate);
    BeanPropertyRowMapper tokenRowMapper = new TokenRawMapper();

    public AuthRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public TokenDTO reqToRegister(UserDTO userDTO) {
        try {
            System.out.println("repository");
            int user_id = tokenRepository.save(userDTO);
            tokenDTO.setToken(tokenService.CreateToken());
            jdbcTemplate.update("INSERT INTO tokens(token, user_id) VALUES(?,?)", tokenDTO.getToken(), user_id);
            return tokenDTO;
        } catch (Exception e) {
            throw new VinylShopExc(ServiceMessage.GENERIC_ERROR);
        }
    }

    public TokenDTO reqToLogin(EntranceDTO entranceDTO) {
        List<Token> tokens;
        try {
            tokens = jdbcTemplate.query("SELECT t.token FROM tokens t JOIN users u ON u.user_id = t.user_id WHERE u.name LIKE '%"
                    + entranceDTO.getName() + "%' AND u.password LIKE '%" + entranceDTO.getPassword() + "%'", tokenRowMapper);
            tokenDTO.setToken(tokens.get(0).getToken());
            return tokenDTO;
        } catch (Exception e) {
            throw new VinylShopExc(ServiceMessage.GENERIC_ERROR);
        }
    }
}
