package ru.vinylshop.repositories;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import ru.vinylshop.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper extends BeanPropertyRowMapper<User> {
    public UserRowMapper() {
        super(User.class);
    }

    @Override
    public User mapRow(ResultSet rs, int rowNumber) throws SQLException {
        User user = super.mapRow(rs, rowNumber);
        return user;
    }
}