package ru.vinylshop.controllers;

import org.springframework.web.bind.annotation.*;
import ru.vinylshop.dto.EntranceDTO;
import ru.vinylshop.dto.TokenDTO;
import ru.vinylshop.dto.UserDTO;
import ru.vinylshop.service.AuthService;

@RestController
@RequestMapping("/auth")
public class AuthController {
    private final AuthService authService;

    public AuthController(AuthService authService){
        this.authService = authService;
    }

    @GetMapping("/login")
    public TokenDTO reqToken(TokenDTO tokenDTO){
        System.out.println("reqToken из AuthController отработал");
        return authService.reqToken(tokenDTO);
    }

    @PostMapping("/login")
    public TokenDTO reqToLogin(@RequestBody EntranceDTO entranceDTO){
        System.out.println("requestToLogin из AuthController отработал");
        return authService.reqToLogin(entranceDTO);
    }

    @DeleteMapping("/logout")
    public void reqToLogout() {
        System.out.println("reqToLogout из AuthController отработал");
        authService.reqToLogout();
    }

    @PostMapping("/reg")
    public TokenDTO reqToRegister(@RequestBody UserDTO userDTO) {
        System.out.println("ReqToRegister из AuthController отработал");
        return authService.reqToRegister(userDTO);
    }
}
