package ru.vinylshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vinylshop.dto.TotalDTO;
import ru.vinylshop.dto.VinylDTO;
import ru.vinylshop.dto.VinylIdDTO;
import ru.vinylshop.service.ShoppingCartService;

import java.util.List;

@RestController
@RequestMapping("/vinyls/shopping-cart")
public class ShoppingCartController {
    private final ShoppingCartService shoppingCartService;

    @Autowired
    public ShoppingCartController(ShoppingCartService shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }

    @GetMapping("/total")
    public TotalDTO buy() {
        System.out.println("buy из ShoppingCartController отработал");
        return shoppingCartService.buy();
    }

    @GetMapping
    public List<VinylDTO> getShoppingCartList() {
        System.out.println("getShoppingCartList из ShoppingCartController отработал");
        return shoppingCartService.getShoppingCartList();
    }

    @PostMapping
    public TotalDTO addToShoppingCart(VinylIdDTO vinylIdDTO) {
        System.out.println("addToShoppingCart из ShoppingCartController отработал");
        return shoppingCartService.addToShoppingCart(vinylIdDTO);
    }

    @DeleteMapping
    public TotalDTO delFromShoppingCart(VinylIdDTO vinylIdDTO) {
        System.out.println("delFromShoppingCart из ShoppingCartController отработал");
        return shoppingCartService.delFromShoppingCart(vinylIdDTO);
    }
}
