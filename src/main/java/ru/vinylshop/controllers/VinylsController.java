package ru.vinylshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vinylshop.dto.VinylDTO;
import ru.vinylshop.dto.VinylIdDTO;
import ru.vinylshop.entity.Vinyl;
import ru.vinylshop.repositories.VinylsRepository;
import ru.vinylshop.service.VinylsService;

import java.util.List;

@RestController
@RequestMapping("/vinyls")
public class VinylsController {

    private final VinylsService vinylsService;

    @Autowired
    public VinylsController(VinylsService vinylsService) {
        this.vinylsService = vinylsService;
    }

    @GetMapping()
    public List<VinylDTO> getVinylList() {
        System.out.println("getVinylList из VinylsController отработал");
        return vinylsService.getVinylList();
    }

    @GetMapping("/favorites")
    public List<VinylDTO> getFavoritesList() {
        System.out.println("getFavoritesList из VinylsController отработал");
        return vinylsService.getFavoritesList();
    }

    @PostMapping("/favorites")
    public VinylDTO putToFavorites(VinylIdDTO vinylIdDTO) {
        System.out.println("putToFavorites из VinylsController отработал");
        return vinylsService.putToFavorites(vinylIdDTO);
    }

    @DeleteMapping("/favorites")
    public void delFromFavorite(VinylIdDTO vinylIdDTO) {
        System.out.println("delFromFavorite из VinylsController отработал");
        vinylsService.delFromFavorite(vinylIdDTO);
    }

    @GetMapping("/search")
    public List<Vinyl> vinylSearch(@RequestParam(value = "name", required = false) String name,
                                   @RequestParam(value = "artist", required = false) String artist,
                                   @RequestParam(value = "genre", required = false) String genre) {
        System.out.println("vinylSearch из VinylsController отработал");
        return vinylsService.vinylSearch(name, artist, genre);
    }
}
