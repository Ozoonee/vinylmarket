package ru.vinylshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vinylshop.dto.PullUserDTO;
import ru.vinylshop.dto.UserDTO;
import ru.vinylshop.service.UsersService;

@RestController
@RequestMapping("/user")
public class UsersController {

    private final UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService){
        this.usersService = usersService;
    }
    @GetMapping("/user_id")
    public UserDTO getCurrentUser(PullUserDTO pullUserDTO) {
        System.out.println("getCurrentUser из UsersController отработал");
        return usersService.getCurrentUser(pullUserDTO);
    }

    @PatchMapping("/user_id")
    public UserDTO editCurrentUser(UserDTO userDTO) {
        System.out.println("editCurrentUser из UsersController отработал");
        return usersService.editCurrentUser(userDTO);
    }
}
