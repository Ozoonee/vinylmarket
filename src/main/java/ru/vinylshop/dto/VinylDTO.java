package ru.vinylshop.dto;

import java.io.Serializable;
import java.util.Objects;

public class VinylDTO implements Serializable {
    private String name;
    private String artist;
    private String genre;

    public VinylDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VinylDTO vinylDTO = (VinylDTO) o;
        return Objects.equals(name, vinylDTO.name) && Objects.equals(artist, vinylDTO.artist) && Objects.equals(genre, vinylDTO.genre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, artist, genre);
    }

    @Override
    public String toString() {
        return "VinylDTO{" +
                "name='" + name + '\'' +
                ", artist='" + artist + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }
}
