package ru.vinylshop.dto;

import java.io.Serializable;
import java.util.Objects;

public class TotalDTO implements Serializable {
    private int vinyl_id;
    private int quantity;
    private double totalPay;

    public TotalDTO() {
    }

    public int getVinyl_id() {
        return vinyl_id;
    }

    public void setVinyl_id(int vinyl_id) {
        this.vinyl_id = vinyl_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotalPay() {
        return totalPay;
    }

    public void setTotalPay(double totalPay) {
        this.totalPay = totalPay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TotalDTO totalDTO = (TotalDTO) o;
        return vinyl_id == totalDTO.vinyl_id && quantity == totalDTO.quantity && Double.compare(totalDTO.totalPay, totalPay) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(vinyl_id, quantity, totalPay);
    }

    @Override
    public String toString() {
        return "TotalDTO{" +
                "vinyl_id=" + vinyl_id +
                ", quantity=" + quantity +
                ", totalPay=" + totalPay +
                '}';
    }
}
