package ru.vinylshop.dto;

import java.io.Serializable;
import java.util.Objects;

public class TokenDTO implements Serializable {
    private String token;

    public TokenDTO() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TokenDTO loginDTO = (TokenDTO) o;
        return Objects.equals(token, loginDTO.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token);
    }

    @Override
    public String toString() {
        return "LoginDTO{" +
                "token='" + token + '\'' +
                '}';
    }
}

