package ru.vinylshop.dto;

import java.io.Serializable;
import java.util.Objects;

public class EntranceDTO implements Serializable {
    private String name;
    private String password;

    public EntranceDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntranceDTO that = (EntranceDTO) o;
        return Objects.equals(name, that.name) && Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, password);
    }

    @Override
    public String toString() {
        return "EntranceDTO{" +
                "name='" + name + '\'' +
                ", pass='" + password + '\'' +
                '}';
    }
}
