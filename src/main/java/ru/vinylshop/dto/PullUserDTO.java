package ru.vinylshop.dto;

import java.io.Serializable;
import java.util.Objects;

public class PullUserDTO implements Serializable {
    private int userId;
    private String token;

    public PullUserDTO() {
    }

    public int getUser_id() {
        return userId;
    }

    public void setUser_id(int user_id) {
        this.userId = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PullUserDTO that = (PullUserDTO) o;
        return userId == that.userId && Objects.equals(token, that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, token);
    }

    @Override
    public String toString() {
        return "PullUserDTO{" +
                "user_id=" + userId +
                ", token='" + token + '\'' +
                '}';
    }
}
