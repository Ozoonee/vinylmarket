package ru.vinylshop.dto;

import java.io.Serializable;
import java.util.Objects;

public class VinylIdDTO implements Serializable {
    private Integer vinylId;

    public VinylIdDTO() {
    }

    public Integer getVinylId() {
        return vinylId;
    }

    public void setVinylId(Integer vinylId) {
        this.vinylId = vinylId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VinylIdDTO that = (VinylIdDTO) o;
        return vinylId == that.vinylId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(vinylId);
    }

    @Override
    public String toString() {
        return "VinylIdDTO{" +
                "vinylId=" + vinylId +
                '}';
    }
}
