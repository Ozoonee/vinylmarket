package ru.vinylshop.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

public class UserDTO implements Serializable {
    private String name;
    private String password;
    private Date birth_Date;
    private String address;
    private String phone;
    private String email;

    public UserDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirth_Date() {
        return birth_Date;
    }

    public void setBirth_Date(Date birth_Date) {
        this.birth_Date = birth_Date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return Objects.equals(name, userDTO.name) && Objects.equals(password, userDTO.password) && Objects.equals(birth_Date,
                userDTO.birth_Date) && Objects.equals(address, userDTO.address) && Objects.equals(phone, userDTO.phone)
                && Objects.equals(email, userDTO.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, password, birth_Date, address, phone, email);
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "name='" + name + '\'' +
                ", pass='" + password + '\'' +
                ", birthDate=" + birth_Date +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", eMail='" + email + '\'' +
                '}';
    }
}
