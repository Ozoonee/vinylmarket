package ru.vinylshop.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.vinylshop.dto.ExceptionDTO;

@RestControllerAdvice
public class CatchExc {

    @ExceptionHandler(VinylShopExc.class)
    private ResponseEntity<ExceptionDTO> handleException(VinylShopExc e) {
        ExceptionDTO excDTO = new ExceptionDTO(e.message);
        return new ResponseEntity<>(excDTO, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    private ResponseEntity<ExceptionDTO> someExc(Exception e) {
        ExceptionDTO excDTO = new ExceptionDTO(ServiceMessage.GENERIC_ERROR);
        return new ResponseEntity<>(excDTO, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AuthException.class)
    private ResponseEntity<ExceptionDTO> AuthExc(Exception e) {
        ExceptionDTO excDTO = new ExceptionDTO(ServiceMessage.AUTH_ERROR);
        return new ResponseEntity<>(excDTO, HttpStatus.UNAUTHORIZED);
    }
}