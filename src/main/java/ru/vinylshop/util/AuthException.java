package ru.vinylshop.util;

public class AuthException extends Exception{
    String message;

    public AuthException(String message) {
        this.message = message;
    }
}
