package ru.vinylshop.util;

public class ServiceMessage {
    public final static String GENERIC_ERROR = "Что то пошло не так, попробуйте позже...";
    public final static String AUTH_ERROR = "Пожалуйста авторизируйтесь";
    public final static String USER_URI = "/user/user_id";
    public final static String VINYLS_URI = "vinyls/favorites";

}
