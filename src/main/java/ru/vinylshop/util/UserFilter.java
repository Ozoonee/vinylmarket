package ru.vinylshop.util;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ru.vinylshop.repositories.TokenRepository.checkUserToken;

@Component
public class UserFilter implements Filter {

    private final HandlerExceptionResolver handlerExceptionResolver;

    public UserFilter(HandlerExceptionResolver handlerExceptionResolver) {
        this.handlerExceptionResolver = handlerExceptionResolver;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String uri = request.getRequestURI();
        try {
            if (uri.equals(ServiceMessage.USER_URI) || uri.equals(ServiceMessage.VINYLS_URI)) {
                boolean aliveToken = checkUserToken(request.getHeader("Token"));
            }
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (AuthException e) {
            handlerExceptionResolver.resolveException(request, response, null, e);
        }
    }
}