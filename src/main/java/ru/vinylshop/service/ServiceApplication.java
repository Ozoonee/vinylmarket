package ru.vinylshop.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(scanBasePackages = "ru.vinylshop")
public class ServiceApplication {
    public static void main(String[] args) {SpringApplication.run(ServiceApplication.class);}
}
