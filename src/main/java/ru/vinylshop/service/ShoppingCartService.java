package ru.vinylshop.service;

import org.springframework.stereotype.Component;
import ru.vinylshop.dto.TotalDTO;
import ru.vinylshop.dto.VinylDTO;
import ru.vinylshop.dto.VinylIdDTO;

import java.util.List;

@Component
public class ShoppingCartService {

    public TotalDTO buy() {
        System.out.println("buy из ShoppingCartService отработал");
        return null;
    }

    public List<VinylDTO> getShoppingCartList() {
        System.out.println("getShoppingCartList из ShoppingCartService отработал");
        return null;
    }

    public TotalDTO addToShoppingCart(VinylIdDTO vinylIdDTO) {
        System.out.println("addToShoppingCart из ShoppingCartService отработал");
        return null;
    }

    public TotalDTO delFromShoppingCart(VinylIdDTO vinylIdDTO) {
        System.out.println("delFromShoppingCart из ShoppingCartService отработал");
        return null;
    }
}
