package ru.vinylshop.service;

import org.springframework.stereotype.Component;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

@Component
public class TokenService {
    BytesToHex bytesToHex = new BytesToHex();

    public TokenService() {
    }

    public String CreateToken() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String digest;
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(UUID.randomUUID().toString().getBytes("UTF-8"));
        return digest = bytesToHex.bytesToHex(md.digest());
    }

    public class BytesToHex {
        public String bytesToHex(byte[] bytes) {
            StringBuilder builder = new StringBuilder();
            for (byte b : bytes) {
                builder.append(String.format("%02x", b));
            }
            return builder.toString();
        }
    }
}

