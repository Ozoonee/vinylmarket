package ru.vinylshop.service;

import org.springframework.stereotype.Component;
import ru.vinylshop.dto.EntranceDTO;
import ru.vinylshop.dto.TokenDTO;
import ru.vinylshop.dto.UserDTO;
import ru.vinylshop.repositories.AuthRepository;

@Component
public class AuthService {
    private final AuthRepository authRepository;

    public AuthService(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    public TokenDTO reqToken(TokenDTO tokenDTO) {
        System.out.println("reqToken из AuthService отработал");
        return null;
    }

    public TokenDTO reqToLogin(EntranceDTO entranceDTO) {
        System.out.println("requestToLogin из AuthService отработал");
        return authRepository.reqToLogin(entranceDTO);
    }

    public void reqToLogout() {
        System.out.println("reqToLogout из AuthService отработал");
    }

    public TokenDTO reqToRegister(UserDTO userDTO) {
        System.out.println("ReqToRegister из AuthService отработал");
        return authRepository.reqToRegister(userDTO);
    }
}
