package ru.vinylshop.service;

import org.springframework.stereotype.Component;
import ru.vinylshop.dto.VinylDTO;
import ru.vinylshop.dto.VinylIdDTO;
import ru.vinylshop.entity.Vinyl;
import ru.vinylshop.repositories.VinylsRepository;

import java.util.ArrayList;
import java.util.List;

@Component
public class VinylsService {
    private final VinylsRepository vinylsRepository;

    public VinylsService(VinylsRepository vinylsRepository) {
        this.vinylsRepository = vinylsRepository;
    }

    public List<VinylDTO> getVinylList() {
        System.out.println("getVinylList из VinylsService отработал");
        List<Vinyl> vinyls = vinylsRepository.getVinylList();

        List<VinylDTO> vinylDTOList = new ArrayList<>();
        for (Vinyl vinyl : vinyls) {
            VinylDTO vinylDTO = new VinylDTO();
            vinylDTO.setArtist(vinyl.getArtist().getName());
            vinylDTO.setName(vinyl.getName());
            vinylDTO.setGenre(vinyl.getGenre().getName());
            vinylDTOList.add(vinylDTO);
        }
        return vinylDTOList;
    }

    public List<VinylDTO> getFavoritesList() {
        System.out.println("getFavoritesList из VinylsService отработал");
        return null;
    }

    public VinylDTO putToFavorites(VinylIdDTO vinylIdDTO) {
        System.out.println("putToFavorites из VinylsService отработал");
        return null;
    }

    public void delFromFavorite(VinylIdDTO vinylIdDTO) {
        System.out.println("delFromFavorite из VinylsService отработал");
    }

    public List<Vinyl> vinylSearch(String name, String artist, String genre) {
        System.out.println("vinylSearch из VinylsService отработал");
        return vinylsRepository.vinylSearch(name, artist, genre);
    }
}
