package ru.vinylshop.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class User implements Serializable {
    private int userId;
    private int vinylsBonuses;
    private String name;
    private String pass;
    private String phone;
    private Date birthDate;

    public User() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getVinylsBonuses() {
        return vinylsBonuses;
    }

    public void setVinylsBonuses(int vinylsBonuses) {
        this.vinylsBonuses = vinylsBonuses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userId == user.userId && vinylsBonuses == user.vinylsBonuses && Objects.equals(name, user.name) && Objects.equals(pass, user.pass) && Objects.equals(phone, user.phone) && Objects.equals(birthDate, user.birthDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, vinylsBonuses, name, pass, phone, birthDate);
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", vinylsBonuses=" + vinylsBonuses +
                ", name='" + name + '\'' +
                ", pass='" + pass + '\'' +
                ", phone='" + phone + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }
}
