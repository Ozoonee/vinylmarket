package ru.vinylshop.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Token implements Serializable {

    private String name;
    private String token;
    private int user_id;
    private Date created_at;
    private int time_to_live;

    public Token() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public int getTime_to_live() {
        return time_to_live;
    }

    public void setTime_to_live(int time_to_live) {
        this.time_to_live = time_to_live;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Token token1 = (Token) o;
        return user_id == token1.user_id && time_to_live == token1.time_to_live && Objects.equals(token, token1.token) && Objects.equals(created_at, token1.created_at);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, user_id, created_at, time_to_live);
    }

    @Override
    public String toString() {
        return "Token{" +
                "token='" + token + '\'' +
                ", userId=" + user_id +
                ", timestamp=" + created_at +
                ", timeToLive=" + time_to_live +
                '}';
    }
}
