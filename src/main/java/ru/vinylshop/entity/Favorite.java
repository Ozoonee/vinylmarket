package ru.vinylshop.entity;

import java.io.Serializable;
import java.util.Objects;

public class Favorite implements Serializable {
    private int favoriteId;
    private int userId;
    private int vinylId;

    public Favorite() {
    }

    public int getFavoriteId() {
        return favoriteId;
    }

    public void setFavoriteId(int favoriteId) {
        this.favoriteId = favoriteId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getVinylId() {
        return vinylId;
    }

    public void setVinylId(int vinylId) {
        this.vinylId = vinylId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Favorite favorite = (Favorite) o;
        return favoriteId == favorite.favoriteId && userId == favorite.userId && vinylId == favorite.vinylId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(favoriteId, userId, vinylId);
    }

    @Override
    public String toString() {
        return "Favorite{" +
                "favoriteId=" + favoriteId +
                ", userId=" + userId +
                ", vinylId=" + vinylId +
                '}';
    }
}
