package ru.vinylshop.entity;

import java.io.Serializable;
import java.util.Objects;

public class OrderItems implements Serializable {
    private int orderItemId;
    private int vinylId;
    private int orderId;

    public OrderItems() {
    }

    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(int orderItemId) {
        this.orderItemId = orderItemId;
    }

    public int getVinylId() {
        return vinylId;
    }

    public void setVinylId(int vinylId) {
        this.vinylId = vinylId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItems that = (OrderItems) o;
        return orderItemId == that.orderItemId && vinylId == that.vinylId && orderId == that.orderId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderItemId, vinylId, orderId);
    }

    @Override
    public String toString() {
        return "OrderItems{" +
                "orderItemId=" + orderItemId +
                ", vinylId=" + vinylId +
                ", orderId=" + orderId +
                '}';
    }
}
