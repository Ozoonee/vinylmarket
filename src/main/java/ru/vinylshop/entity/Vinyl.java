package ru.vinylshop.entity;

import java.io.Serializable;
import java.util.Objects;

public class Vinyl implements Serializable {
    private String name;
    private int vinylId;
    private Artist artist;
    private int artistId;
    private int bonus;
    private int price;
    private Genre genre;
    private int genre_id;

    public Vinyl() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVinylId() {
        return vinylId;
    }

    public void setVinylId(int vinylId) {
        this.vinylId = vinylId;
    }

    public int getArtistId() {
        return artistId;
    }

    public void setArtistId(int artistId) {
        this.artistId = artistId;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public int getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(int genre_id) {
        this.genre_id = genre_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vinyl vinyl = (Vinyl) o;
        return vinylId == vinyl.vinylId && artistId == vinyl.artistId && bonus == vinyl.bonus && price == vinyl.price && genre_id == vinyl.genre_id && Objects.equals(name, vinyl.name) && Objects.equals(artist, vinyl.artist) && Objects.equals(genre, vinyl.genre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, vinylId, artist, artistId, bonus, price, genre, genre_id);
    }

    @Override
    public String toString() {
        return "Vinyl{" +
                "name='" + name + '\'' +
                ", vinylId=" + vinylId +
                ", artist=" + artist +
                ", artistId=" + artistId +
                ", bonus=" + bonus +
                ", price=" + price +
                ", genre=" + genre +
                ", genre_id=" + genre_id +
                '}';
    }
}